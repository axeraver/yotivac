# Yoti Robot Vacuum

This project contains a simple Spring Boot application that enables navigation of an imaginary robot vacuum.

### Building

This project requires java 11+ to be compiled and run.

Clone this project to your local device.
If not on the system path the full path to the java home directory should be set in the first instance.

e.g.
set JAVA_HOME="C:\Program Files\Java\jdk-11\"

Open a terminal at the root project directory e.g. c:\users\jak\projects\yotivac

To build the jar and run the unit tests run the buildNeeded gradle task:

<code>./gradlew buildNeeded</code>

To just compile the source and run the unit tests enter:

<code>./gradlew test</code>


---


The boot jar will be generated into the build\lib directory e.g. c:\users\jak\projects\yotivac\build\libs\yotivac-1.0-SNAPSHOT.jar

Staying in the home directory this can be run as

java -jar build\libs\yotivac-1.0-SNAPSHOT.jar

By default the server will listen port 8888.

http://localhost:8989/

To have the server listen on a different port pass the server-port argument on the command line e.g. :

java -jar build\libs\yotivac-1.0-SNAPSHOT.jar --server.port=9999

or update the sever.port value in resources/application.properties before building

---

### API Endpoint

The endpoint to pass the payload to the robot vacuum is:

http://localhost:8888/api/cleaning

---

For demonstration purposes only this project is using embedded mongo for the database. Uncommenting the last two lines in application.properties will allow the mongodb data to be persisted between server starts, but will cause test failures when the tests are run via gradle.

