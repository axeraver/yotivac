package com.jak.yoti.yotivac.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;


@Document(collection = "CleaningJob")
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CleaningJob {

    @Id
    private String id;
    private int[] roomSize;
    private int[] startPosition;
    private List<int[]> dirt;
    private String instructions;
    private int cleaned;
    private int[] endPosition;

}
