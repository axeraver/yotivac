package com.jak.yoti.yotivac.repository;

import com.jak.yoti.yotivac.domain.CleaningJob;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CleaningJobRepository extends MongoRepository<CleaningJob, String> {
}
