package com.jak.yoti.yotivac.controller;

import com.jak.yoti.yotivac.exception.InvalidPositionException;
import com.jak.yoti.yotivac.exception.InvalidRoomSizeException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class CustomResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({ InvalidPositionException.class })
    public ResponseEntity<Object> handleExistsException(Exception exception, WebRequest request) {
        return new ResponseEntity <>("Coordinates are not in the room", new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({ InvalidRoomSizeException.class })
    public ResponseEntity<Object> handleInvalidInputException(Exception exception, WebRequest request) {
        return new ResponseEntity <>("Each room dimension must be greater than 0", new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

}