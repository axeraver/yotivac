package com.jak.yoti.yotivac.controller;

import com.jak.yoti.yotivac.dto.CleaningDoneDTO;
import com.jak.yoti.yotivac.dto.CleaningJobDTO;
import com.jak.yoti.yotivac.dto.CreatedJobDTO;
import com.jak.yoti.yotivac.exception.InvalidInputException;
import com.jak.yoti.yotivac.service.CleaningJobService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Log4j2
@RestController
@RequestMapping(path = "/api/cleaning", produces = MediaType.APPLICATION_JSON_VALUE)
public class CleaningJobController {

    @Autowired
    private CleaningJobService cleaningJobService;

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public CleaningDoneDTO cleanRoom(@Valid @RequestBody CleaningJobDTO cleaningJobDTO, BindingResult bindingResult) {
        checkValidation(bindingResult);
        return cleaningJobService.cleanRoom(cleaningJobDTO);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<CreatedJobDTO> getCreatedJobs() {
        return cleaningJobService.getAllCreatedJobs();
    }

    private void checkValidation(BindingResult bindingResult) {
        if(bindingResult.hasErrors()) {
            if (bindingResult.hasFieldErrors() && bindingResult.getFieldError() != null) {
                log.error("Invalid input found: {}", bindingResult.getFieldError());
                throw new InvalidInputException("Request body has invalid data: " + bindingResult.getFieldError().getField() + " - " + bindingResult.getFieldError().getDefaultMessage());
            } else {
                log.error("Invalid input passed in request body");
                throw new InvalidInputException();
            }
        }
    }

}
