package com.jak.yoti.yotivac;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YotivacApplication {

    public static void main(String[] args) {
        SpringApplication.run(YotivacApplication.class, args);
    }

}
