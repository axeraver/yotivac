package com.jak.yoti.yotivac.dto;

import com.jak.yoti.yotivac.validation.DirectionConstraint;
import com.jak.yoti.yotivac.validation.PositionConstraint;
import com.jak.yoti.yotivac.validation.RoomSizeConstraint;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CleaningJobDTO implements Serializable {

    @RoomSizeConstraint
    private int[] roomSize; // expecting two positive ints

    @PositionConstraint
    private int[] coords; // expecting two positive ints within the room

    @PositionConstraint
    private List<int[]> patches; // expecting a list of unique coordinates within the room

    @DirectionConstraint
    private String instructions; // expecting a string containing 'n', 's', 'e', 'w', 'N', 'S', 'E' or 'W'

}
