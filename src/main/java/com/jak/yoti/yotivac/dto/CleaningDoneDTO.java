package com.jak.yoti.yotivac.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CleaningDoneDTO {

    private int[] coords;
    private int patches;

}
