package com.jak.yoti.yotivac.dto;

import lombok.Value;

@Value
public class CreatedJobDTO {

    String id;
    CleaningJobDTO cleaningJob;
    CleaningDoneDTO cleaningDone;

}
