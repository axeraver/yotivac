package com.jak.yoti.yotivac.robovac;

import com.jak.yoti.yotivac.exception.InvalidRoomSizeException;
import lombok.Getter;

import java.util.List;

import static org.springframework.util.CollectionUtils.isEmpty;

@Getter
public class Room {

    private final int x;
    private final int y;
    private final List<Position> dirt;
    private int cleaned = 0;

    public Room(int[] size, List<Position> dirt) {
        this.x = size[0];
        this.y = size[1];
        this.dirt = dirt;
    }

    public Room(int x, int y, List<Position> dirt) {
        if(x <= 0 || y <= 0) {
            throw new InvalidRoomSizeException();
        }
        this.x = x;
        this.y = y;
        this.dirt = dirt;
    }

    public boolean isValidPosition(Position position) {
        return isValidPosition(position.getX(), position.getY());
    }

    public boolean isValidPosition(int newX, int newY) {
        return newX >= 0 && newX <= x && newY >= 0 && newY <= y;
    }

    public void clean(Position position) {
        // hopefully the vac only needs one attempt to clean a dirty sport, so remove from dirt
        // list when first visited
        if(!isEmpty(dirt) && dirt.removeIf(p -> p.equals(position))) {
            cleaned++;
        }
    }

}
