package com.jak.yoti.yotivac.robovac;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Objects;

@Data
@AllArgsConstructor
public class Position {

    private int x;
    private int y;

    public Position(int[] coords) {
        this.x = coords[0];
        this.y = coords[1];
    }

    public void update(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (!(o instanceof Position)) return false;
        Position other = (Position) o;
        return (this.x == other.x && this.y == other.y);
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }
}
