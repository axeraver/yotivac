package com.jak.yoti.yotivac.robovac;

import com.jak.yoti.yotivac.exception.InvalidPositionException;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Getter
public class RoboVac {

    private final Room room;
    private final Position currentPosition;

    public RoboVac(Room room, Position currentPosition) {
        this.room = room;
        if(!room.isValidPosition(currentPosition)) {
            throw new InvalidPositionException();
        }
        this.currentPosition = currentPosition;
    }

    public void move(char direction) {
        switch(direction) {
            case 'n':
            case 'N': moveNorth(); break;
            case 's':
            case 'S': moveSouth(); break;
            case 'e':
            case 'E': moveEast(); break;
            case 'w':
            case 'W': moveWest(); break;
            default:
                log.warn("Invalid direction specified: {}", direction);
                break;
        }
    }

    private void moveNorth() {
        log.debug("Attempting to move north");
        cleanIfMoved(currentPosition.getX(), currentPosition.getY() + 1);
    }

    private void moveSouth() {
        log.debug("Attempting to move south");
        cleanIfMoved(currentPosition.getX(), currentPosition.getY() - 1);
    }

    private void moveEast() {
        log.debug("Attempting to move east");
        cleanIfMoved(currentPosition.getX() + 1, currentPosition.getY());
    }

    private void moveWest() {
        log.debug("Attempting to move west");
        cleanIfMoved(currentPosition.getX() - 1, currentPosition.getY());
    }

    private void cleanIfMoved(int newX, int newY) {
        if(room.isValidPosition(newX, newY)) {
            currentPosition.update(newX, newY);
            room.clean(currentPosition);
            log.info("Moved to {} and cleaned", currentPosition);
        } else {
            log.warn("Unable to move to new position due to room constraints. Staying at {}", currentPosition);
            throw new InvalidPositionException();
        }
    }


}
