package com.jak.yoti.yotivac.service;

import com.jak.yoti.yotivac.dto.CleaningDoneDTO;
import com.jak.yoti.yotivac.dto.CleaningJobDTO;
import com.jak.yoti.yotivac.dto.CreatedJobDTO;

import java.util.List;

public interface CleaningJobService {

    CleaningDoneDTO cleanRoom(CleaningJobDTO cleaningJobDTO);

    List<CreatedJobDTO> getAllCreatedJobs();

}
