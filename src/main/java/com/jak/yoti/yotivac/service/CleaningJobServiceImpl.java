package com.jak.yoti.yotivac.service;

import com.jak.yoti.yotivac.domain.CleaningJob;
import com.jak.yoti.yotivac.dto.CleaningDoneDTO;
import com.jak.yoti.yotivac.dto.CleaningJobDTO;
import com.jak.yoti.yotivac.dto.CreatedJobDTO;
import com.jak.yoti.yotivac.mapper.CleaningJobMapper;
import com.jak.yoti.yotivac.repository.CleaningJobRepository;
import com.jak.yoti.yotivac.robovac.Position;
import com.jak.yoti.yotivac.robovac.RoboVac;
import com.jak.yoti.yotivac.robovac.Room;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Log4j2
@Service
public class CleaningJobServiceImpl implements CleaningJobService {

    @Autowired
    private CleaningJobMapper cleaningJobMapper;

    @Autowired
    private CleaningJobRepository cleaningJobRepository;

    @Override
    public CleaningDoneDTO cleanRoom(CleaningJobDTO cleaningJobDTO) {
        CleaningJob cleaningJob = cleaningJobMapper.cleaningJobDTOtoCleaningJob(cleaningJobDTO);

        List<Position> dirt = cleaningJobMapper.coordsToPositions(cleaningJobDTO.getPatches());
        Room roomToClean = new Room(cleaningJobDTO.getRoomSize(), dirt);
        Position startPosition = cleaningJobMapper.arrayToPosition(cleaningJobDTO.getCoords());
        RoboVac roboVac = new RoboVac(roomToClean, startPosition);
        for(char direction : cleaningJobDTO.getInstructions().toCharArray()) {
            roboVac.move(direction);
        }

        final int[] endCoords = cleaningJobMapper.positionToArray(roboVac.getCurrentPosition());
        final int cleaned = roomToClean.getCleaned();

        cleaningJob.setCleaned(cleaned);
        cleaningJob.setEndPosition(endCoords);

        cleaningJobRepository.save(cleaningJob);

        return cleaningJobMapper.cleaningJobToCleaningDoneDTO(cleaningJob);
    }

    @Override
    public List<CreatedJobDTO> getAllCreatedJobs() {
        List<CreatedJobDTO> finishedJobs = new ArrayList<>();
        try {
            List<CleaningJob> jobs = cleaningJobRepository.findAll();
            finishedJobs = jobs.stream().map(j -> cleaningJobMapper.cleaningJobToCreatedJobDTO(j)).collect(Collectors.toList());
        } catch (Exception e) {
            log.error("Could not create list of job history - {}", e.getMessage());
        }
        return finishedJobs;
    }
}
