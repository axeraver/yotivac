package com.jak.yoti.yotivac.mapper;

import com.jak.yoti.yotivac.domain.CleaningJob;
import com.jak.yoti.yotivac.dto.CleaningDoneDTO;
import com.jak.yoti.yotivac.dto.CleaningJobDTO;
import com.jak.yoti.yotivac.dto.CreatedJobDTO;
import com.jak.yoti.yotivac.robovac.Position;

import java.util.List;

public interface CleaningJobMapper {

    CleaningJobDTO cleaningJobToCleaningJobDTO(CleaningJob cleaningJob);

    CleaningJob cleaningJobDTOtoCleaningJob(CleaningJobDTO cleaningJobDTO);

    CleaningDoneDTO cleaningJobToCleaningDoneDTO(CleaningJob cleaningJob);

    Position arrayToPosition(int[] coords);

    int[] positionToArray(Position position);

    List<Position> coordsToPositions(List<int[]> coords);

    CreatedJobDTO cleaningJobToCreatedJobDTO(CleaningJob cleaningJob);

}
