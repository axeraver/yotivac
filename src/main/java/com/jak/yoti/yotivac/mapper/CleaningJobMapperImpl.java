package com.jak.yoti.yotivac.mapper;

import com.jak.yoti.yotivac.domain.CleaningJob;
import com.jak.yoti.yotivac.dto.CleaningDoneDTO;
import com.jak.yoti.yotivac.dto.CleaningJobDTO;
import com.jak.yoti.yotivac.dto.CreatedJobDTO;
import com.jak.yoti.yotivac.robovac.Position;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CleaningJobMapperImpl implements CleaningJobMapper {

    @Override
    public CleaningJob cleaningJobDTOtoCleaningJob(CleaningJobDTO cleaningJobDTO) {
        return CleaningJob.builder()
                .roomSize(cleaningJobDTO.getRoomSize())
                .startPosition(cleaningJobDTO.getCoords())
                .dirt(cleaningJobDTO.getPatches())
                .instructions(cleaningJobDTO.getInstructions())
                .build();
    }

    @Override
    public CleaningJobDTO cleaningJobToCleaningJobDTO(CleaningJob cleaningJob) {
        return CleaningJobDTO.builder()
                .roomSize(cleaningJob.getRoomSize())
                .coords(cleaningJob.getStartPosition())
                .patches(cleaningJob.getDirt())
                .instructions(cleaningJob.getInstructions())
                .build();
    }

    @Override
    public CleaningDoneDTO cleaningJobToCleaningDoneDTO(CleaningJob cleaningJob) {
        return CleaningDoneDTO.builder()
                .coords(cleaningJob.getEndPosition())
                .patches(cleaningJob.getCleaned())
                .build();
    }

    @Override
    public CreatedJobDTO cleaningJobToCreatedJobDTO(CleaningJob cleaningJob) {
        CleaningJobDTO cleaningJobDTO = cleaningJobToCleaningJobDTO(cleaningJob);
        CleaningDoneDTO cleaningDoneDTO = cleaningJobToCleaningDoneDTO(cleaningJob);
        return new CreatedJobDTO(cleaningJob.getId(), cleaningJobDTO, cleaningDoneDTO);
    }

    @Override
    public Position arrayToPosition(int[] coords) {
        if(coords.length == 2) {
            return new Position(coords[0], coords[1]);
        }
        return null;
    }

    @Override
    public int[] positionToArray(Position position) {
        return new int[]{position.getX(), position.getY()};
    }

    @Override
    public List<Position> coordsToPositions(List<int[]> coords) {
        return coords.stream().map(this::arrayToPosition).collect(Collectors.toList());
    }

}
