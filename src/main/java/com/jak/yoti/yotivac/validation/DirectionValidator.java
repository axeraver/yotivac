package com.jak.yoti.yotivac.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class DirectionValidator implements ConstraintValidator<DirectionConstraint, String> {

    @Override
    public void initialize(DirectionConstraint constraintAnnotation) {
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return value != null && value.matches("^[nsewNSEW]+$");
    }

}
