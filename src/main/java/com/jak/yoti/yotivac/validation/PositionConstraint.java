package com.jak.yoti.yotivac.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = { PositionValidator.class, PositionListValidator.class })
@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface PositionConstraint {
    String message() default "Invalid RoboVac position";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
