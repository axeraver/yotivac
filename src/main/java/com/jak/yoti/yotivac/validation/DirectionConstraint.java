package com.jak.yoti.yotivac.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = DirectionValidator.class)
@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface DirectionConstraint {
    String message() default "Invalid RoboVac direction - must be 'N', 'S', 'W' or 'E'";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
