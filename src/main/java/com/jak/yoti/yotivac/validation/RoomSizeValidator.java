package com.jak.yoti.yotivac.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class RoomSizeValidator implements ConstraintValidator<RoomSizeConstraint, int[]> {

    @Override
    public void initialize(RoomSizeConstraint constraintAnnotation) {
    }

    @Override
    public boolean isValid(int[] value, ConstraintValidatorContext context) {
        return value != null && value.length == 2 && value[0] > 0 && value[1] > 0;
    }
}
