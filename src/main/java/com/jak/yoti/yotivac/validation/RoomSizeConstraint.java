package com.jak.yoti.yotivac.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = RoomSizeValidator.class)
@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface RoomSizeConstraint {
    String message() default "Invalid room size for RoboVac";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
