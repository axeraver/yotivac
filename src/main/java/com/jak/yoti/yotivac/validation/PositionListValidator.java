package com.jak.yoti.yotivac.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.List;

public class PositionListValidator  implements ConstraintValidator<PositionConstraint, List<int[]>> {

    @Override
    public void initialize(PositionConstraint constraintAnnotation) {
    }

    @Override
    public boolean isValid(List<int[]> value, ConstraintValidatorContext context) {
        return value != null && value.stream().allMatch(v -> v.length == 2 && v[0] >= 0 && v[1] >= 0);
    }

}