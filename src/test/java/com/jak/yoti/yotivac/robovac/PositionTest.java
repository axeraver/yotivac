package com.jak.yoti.yotivac.robovac;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class PositionTest {

    @Test
    void testConstructor() {
        Position p = new Position(0, 0);
        assertEquals(0, p.getX());
        assertEquals(0, p.getY());
    }

    @Test
    void testUpdate() {
        Position p = new Position(0, 0);
        p.update(0, 1);
        assertEquals(0, p.getX());
        assertEquals(1, p.getY());
        p.update(2, 4);
        assertEquals(2, p.getX());
        assertEquals(4, p.getY());
    }

    @Test
    void testEquals() {
        Position p1 = new Position(2, 4);
        Position p2 = new Position(4, 6);
        assertNotEquals(p1, p2);
        p2.update(2, 4);
        assertEquals(p1, p2);
    }

    @Test
    void testHashCode() {
        Position p1 = new Position(1, 5);
        Position p2 = new Position(3, 5);
        int p1hash = p1.hashCode();
        int p2hash = p2.hashCode();
        assertNotEquals(0, p1hash);
        assertNotEquals(0, p2hash);
        assertNotEquals(p1hash, p2hash);
    }

    @Test
    void testToString() {
        Position p = new Position(5, 4);
        assertEquals("Position(x=5, y=4)", p.toString());
    }
}