package com.jak.yoti.yotivac.robovac;

import com.jak.yoti.yotivac.exception.InvalidPositionException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class RoboVacTest {

    private final Position startPosition = new Position(2, 2);
    private final Room cleanRoom = new Room(4, 4, Collections.emptyList());

    private RoboVac classUnderTest;

    @BeforeEach
    public void initRoboVac() {
        classUnderTest = new RoboVac(cleanRoom, startPosition);
    }

    @Test
    void testConstructor() {
        assertEquals(cleanRoom, classUnderTest.getRoom());
        assertEquals(startPosition, classUnderTest.getCurrentPosition());
    }

    @Test
    void testConstructorWithInvalidStartPosition() {
        assertThrows(InvalidPositionException.class, () -> new RoboVac(cleanRoom, new Position(5, 1)));
        assertThrows(InvalidPositionException.class, () -> new RoboVac(cleanRoom, new Position(-5, 1)));
        assertThrows(InvalidPositionException.class, () -> new RoboVac(cleanRoom, new Position(1, 5)));
        assertThrows(InvalidPositionException.class, () -> new RoboVac(cleanRoom, new Position(1, -5)));
    }

    @Test
    void testMoveInvalidDirection() {
        classUnderTest.move('R');
        assertEquals(startPosition, classUnderTest.getCurrentPosition());
    }

    @Test
    void testMoveNorth() {
        classUnderTest.move('N');
        assertEquals(new Position(2, 3), classUnderTest.getCurrentPosition());
        classUnderTest.move('n');
        assertEquals(new Position(2, 4), classUnderTest.getCurrentPosition());
    }

    @Test
    void testMoveSouth() {
        classUnderTest.move('S');
        assertEquals(new Position(2, 1), classUnderTest.getCurrentPosition());
        classUnderTest.move('s');
        assertEquals(new Position(2, 0), classUnderTest.getCurrentPosition());
    }

    @Test
    void testMoveEast() {
        classUnderTest.move('E');
        assertEquals(new Position(3, 2), classUnderTest.getCurrentPosition());
        classUnderTest.move('e');
        assertEquals(new Position(4, 2), classUnderTest.getCurrentPosition());
    }

    @Test
    void testMoveWest() {
        classUnderTest.move('W');
        assertEquals(new Position(1, 2), classUnderTest.getCurrentPosition());
        classUnderTest.move('w');
        assertEquals(new Position(0, 2), classUnderTest.getCurrentPosition());
    }

    @Test
    void testValidMoveCleansPosition() {
        Room mockRoom = mock(Room.class);
        when(mockRoom.isValidPosition(startPosition)).thenReturn(true);
        when(mockRoom.isValidPosition(3, 2)).thenReturn(true);
        RoboVac roboVac = new RoboVac(mockRoom, startPosition);
        roboVac.move('E');
        verify(mockRoom, times(1)).isValidPosition(3, 2);
        verify(mockRoom, times(1)).clean(new Position(3, 2));
    }

    @Test
    void testInvalidMoveDoesNotClean() {
        Room mockRoom = mock(Room.class);
        when(mockRoom.isValidPosition(startPosition)).thenReturn(true);
        when(mockRoom.isValidPosition(2, 3)).thenReturn(false);
        RoboVac roboVac = new RoboVac(mockRoom, startPosition);
        assertThrows(InvalidPositionException.class, () -> roboVac.move('N'));
        verify(mockRoom, times(1)).isValidPosition(2, 3);
        verify(mockRoom, never()).clean(any(Position.class));
    }
}