package com.jak.yoti.yotivac.robovac;

import com.jak.yoti.yotivac.exception.InvalidRoomSizeException;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class RoomTest {

    private Room classUnderTest;

    @Test
    void testConstructor() {
        Room room = new Room(2, 3, Collections.emptyList());
        assertEquals(2, room.getX());
        assertEquals(3, room.getY());
        assertTrue(room.getDirt().isEmpty());
        assertEquals(0, room.getCleaned());
    }

    @Test
    void testConstructorWithInvalidSize() {
        assertThrows(InvalidRoomSizeException.class, () -> new Room(0, 1, Collections.emptyList()));
        assertThrows(InvalidRoomSizeException.class, () -> new Room(-2, 1, Collections.emptyList()));
        assertThrows(InvalidRoomSizeException.class, () -> new Room(1, 0, Collections.emptyList()));
        assertThrows(InvalidRoomSizeException.class, () -> new Room(1, -2, Collections.emptyList()));
        assertThrows(InvalidRoomSizeException.class, () -> new Room(0, 0, Collections.emptyList()));
        assertThrows(InvalidRoomSizeException.class, () -> new Room(-2, -2, Collections.emptyList()));
    }

    @Test
    void testIsValidPosition() {
        Room room = new Room(1, 1, Collections.emptyList());
        assertTrue(room.isValidPosition(0, 0));
        assertTrue(room.isValidPosition(0, 1));
        assertTrue(room.isValidPosition(1, 0));
        assertTrue(room.isValidPosition(1, 1));
        assertFalse(room.isValidPosition(0, 2));
        assertFalse(room.isValidPosition(2, 0));
        assertFalse(room.isValidPosition(2, 2));
        assertFalse(room.isValidPosition(-1, 0));
        assertFalse(room.isValidPosition(0, -1));
    }

    @Test
    void testCleanWithNoDirt() {
        Room room = new Room(2, 2, Collections.emptyList());
        assertEquals(0, room.getCleaned());
        room.clean(new Position(1, 1));
        assertEquals(0, room.getCleaned());
    }

    @Test
    void testCleanDirtyRoom() {
        List<Position> dirt = new ArrayList<>(Arrays.asList(new Position(0, 0), new Position(0, 1), new Position(1, 0), new Position(1, 1)));
        Room room = new Room(1, 1, dirt);
        assertEquals(0, room.getCleaned());
        assertEquals(4, dirt.size());
        room.clean(new Position(0, 0));
        room.clean(new Position(1, 0));
        room.clean(new Position(0, 1));
        room.clean(new Position(1, 1));
        assertEquals(4, room.getCleaned());
        assertTrue(dirt.isEmpty());
    }

}