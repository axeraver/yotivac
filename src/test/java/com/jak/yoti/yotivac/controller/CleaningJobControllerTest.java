package com.jak.yoti.yotivac.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jak.yoti.yotivac.dto.CleaningDoneDTO;
import com.jak.yoti.yotivac.dto.CleaningJobDTO;
import com.jak.yoti.yotivac.service.CleaningJobService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class CleaningJobControllerTest {

    @MockBean
    private CleaningJobService cleaningJobService;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void testPostCleaningJob() throws Exception {
        CleaningJobDTO cleaningJobDTO = CleaningJobDTO.builder()
                .coords(new int[]{1, 2})
                .roomSize(new int[]{5, 5})
                .patches(Arrays.asList(new int[]{1, 2}, new int[]{2, 2}))
                .instructions("NNEESSWWNES")
                .build();

        CleaningDoneDTO cleaningDoneDTO = new CleaningDoneDTO(new int[]{1, 2}, 2);

        when(cleaningJobService.cleanRoom(any(CleaningJobDTO.class))).thenReturn(cleaningDoneDTO);

        mockMvc.perform(post("/api/cleaning")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(cleaningJobDTO)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.coords", hasSize(2)))
                .andExpect(jsonPath("$.coords[0]", equalTo(1)))
                .andExpect(jsonPath("$.coords[1]", equalTo(2)))
                .andExpect(jsonPath("$.patches", equalTo(2)));
    }

}