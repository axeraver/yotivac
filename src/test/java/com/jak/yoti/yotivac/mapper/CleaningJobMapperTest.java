package com.jak.yoti.yotivac.mapper;

import com.jak.yoti.yotivac.domain.CleaningJob;
import com.jak.yoti.yotivac.dto.CleaningDoneDTO;
import com.jak.yoti.yotivac.dto.CleaningJobDTO;
import com.jak.yoti.yotivac.dto.CreatedJobDTO;
import com.jak.yoti.yotivac.robovac.Position;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CleaningJobMapperTest {

    CleaningJobMapper cleaningJobMapper = new CleaningJobMapperImpl();

    @Test
    void testMapCleaningJobToCleaningJobDTO() {
        CleaningJob cleaningJob = CleaningJob.builder()
                .instructions("NSEW")
                .roomSize(new int[]{2,4})
                .dirt(Arrays.asList(new int[]{1, 1}, new int[]{2, 2}))
                .startPosition(new int[]{3, 3})
                .build();

        CleaningJobDTO cleaningJobDTO = cleaningJobMapper.cleaningJobToCleaningJobDTO(cleaningJob);

        assertEquals(2, cleaningJobDTO.getRoomSize()[0]);
        assertEquals(4, cleaningJobDTO.getRoomSize()[1]);
        assertEquals(1, cleaningJobDTO.getPatches().get(0)[0]);
        assertEquals(1, cleaningJobDTO.getPatches().get(0)[1]);
        assertEquals(2, cleaningJobDTO.getPatches().get(1)[0]);
        assertEquals(2, cleaningJobDTO.getPatches().get(1)[1]);
        assertEquals(3, cleaningJobDTO.getCoords()[0]);
        assertEquals(3, cleaningJobDTO.getCoords()[1]);
        assertEquals("NSEW", cleaningJobDTO.getInstructions());
    }

    @Test
    void testMapCleaningJobDTOToCleaningJob() {
        CleaningJobDTO cleaningJobDTO = CleaningJobDTO.builder()
                .coords(new int[]{2, 3})
                .patches(Arrays.asList(new int[]{1, 1}, new int[]{2, 3}))
                .instructions("NNEESSWW")
                .roomSize(new int[]{5, 6})
                .build();

        CleaningJob cleaningJob = cleaningJobMapper.cleaningJobDTOtoCleaningJob(cleaningJobDTO);

        assertEquals("NNEESSWW", cleaningJob.getInstructions());
        assertEquals(2, cleaningJob.getStartPosition()[0]);
        assertEquals(3, cleaningJob.getStartPosition()[1]);
        assertEquals(5, cleaningJob.getRoomSize()[0]);
        assertEquals(6, cleaningJob.getRoomSize()[1]);
        assertEquals(1, cleaningJob.getDirt().get(0)[0]);
        assertEquals(1, cleaningJob.getDirt().get(0)[1]);
        assertEquals(2, cleaningJob.getDirt().get(1)[0]);
        assertEquals(3, cleaningJob.getDirt().get(1)[1]);
    }

    @Test
    void testCleaningJobToCleaningDoneDTO() {
        CleaningJob cleaningJob = CleaningJob.builder()
                .instructions("NSEW")
                .roomSize(new int[]{2,4})
                .dirt(Arrays.asList(new int[]{1, 1}, new int[]{2, 2}))
                .startPosition(new int[]{3, 3})
                .cleaned(1)
                .endPosition(new int[]{2, 2})
                .build();

        CleaningDoneDTO cleaningDoneDTO = cleaningJobMapper.cleaningJobToCleaningDoneDTO(cleaningJob);

        assertEquals(1, cleaningDoneDTO.getPatches());
        assertEquals(2, cleaningDoneDTO.getCoords()[0]);
        assertEquals(2, cleaningDoneDTO.getCoords()[1]);
    }

    @Test
    void testCleaningJobToCreatedJobDTO() {
        CleaningJob cleaningJob = CleaningJob.builder()
                .id("junit_test")
                .instructions("NSEW")
                .roomSize(new int[]{2,4})
                .dirt(Arrays.asList(new int[]{1, 1}, new int[]{2, 2}))
                .startPosition(new int[]{3, 3})
                .cleaned(1)
                .endPosition(new int[]{2, 2})
                .build();

        CreatedJobDTO createdJobDTO = cleaningJobMapper.cleaningJobToCreatedJobDTO(cleaningJob);

        assertEquals("junit_test", createdJobDTO.getId());

        CleaningJobDTO cleaningJobDTO = createdJobDTO.getCleaningJob();

        assertEquals(2, cleaningJobDTO.getRoomSize()[0]);
        assertEquals(4, cleaningJobDTO.getRoomSize()[1]);
        assertEquals(1, cleaningJobDTO.getPatches().get(0)[0]);
        assertEquals(1, cleaningJobDTO.getPatches().get(0)[1]);
        assertEquals(2, cleaningJobDTO.getPatches().get(1)[0]);
        assertEquals(2, cleaningJobDTO.getPatches().get(1)[1]);
        assertEquals(3, cleaningJobDTO.getCoords()[0]);
        assertEquals(3, cleaningJobDTO.getCoords()[1]);
        assertEquals("NSEW", cleaningJobDTO.getInstructions());

        CleaningDoneDTO cleaningDoneDTO = createdJobDTO.getCleaningDone();

        assertEquals(1, cleaningDoneDTO.getPatches());
        assertEquals(2, cleaningDoneDTO.getCoords()[0]);
        assertEquals(2, cleaningDoneDTO.getCoords()[1]);
    }

    @Test
    void testArrayToPosition() {
        Position p = cleaningJobMapper.arrayToPosition(new int[]{2, 5});
        assertEquals(2, p.getX());
        assertEquals(5, p.getY());
    }

    @Test
    void testPositionToArray() {
        int[] coords = cleaningJobMapper.positionToArray(new Position(4, 1));
        assertEquals(4, coords[0]);
        assertEquals(1, coords[1]);
    }

    @Test
    void testCoordsToPositions() {
        List<Position> positions = cleaningJobMapper.coordsToPositions(Arrays.asList(new int[]{2, 3}, new int[]{4, 1}));
        assertEquals(new Position(2, 3), positions.get(0));
        assertEquals(new Position(4, 1), positions.get(1));
    }
}
